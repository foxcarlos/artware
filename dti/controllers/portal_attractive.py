# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.addons.portal.controllers.portal import CustomerPortal, \
    pager as portal_pager
from odoo.exceptions import AccessError, MissingError
from odoo.http import request


class PortalAttraction(CustomerPortal):

    def _prepare_home_portal_values(self, counters):
        values = super()._prepare_home_portal_values(counters)
        attraction_entity = http.request.env.user.partner_id.\
            know_entity_from_partner()
        attraction_ = attraction_entity.attraction_ids.ids if \
            attraction_entity else []
        domain = [('id', 'in', attraction_)]

        if 'attraction_count' in counters:
            attraction_count = request.env['dti.attraction'].sudo().\
                search_count(domain)
            values['attraction_count'] = attraction_count
        return values

    # ------------------------------------------------------------
    # My attractions
    # ------------------------------------------------------------

    def _attraction_get_page_view_values(self, attraction, access_token,
                                         **kwargs):
        values = {
            'page_name': 'Attraction',
            'attraction': attraction,
        }
        return self._get_page_view_values(attraction, access_token, values,
                                          'my_attractions_history', False,
                                          **kwargs)

    @http.route(['/my/attractions', '/my/attractions/page/<int:page>'],
                type='http', auth="user", website=True)
    def portal_my_attractions(self, page=1, date_begin=None, date_end=None,
                              sortby=None, filterby=None, **kw):
        values = self._prepare_portal_layout_values()
        DtiAttraction = request.env['dti.attraction']

        attraction_entity = http.request.env.user.partner_id.\
            know_entity_from_partner()
        attraction_ = attraction_entity.attraction_ids.ids if \
            attraction_entity else []
        domain = [('id', 'in', attraction_)]

        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Name'), 'order': 'name'},
        }

        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']

        # count for pager
        attraction_count = DtiAttraction.sudo().search_count(domain)
        # pager
        pager = portal_pager(
            url="/my/attractions",
            url_args={'date_begin': date_begin, 'date_end': date_end,
                      'sortby': sortby},
            total=attraction_count,
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        attractions = DtiAttraction.sudo().search(domain, order=order,
                                                  limit=self._items_per_page,
                                                  offset=pager['offset'])
        request.session['my_attractions_history'] = attractions.ids[:100]

        values.update({
            'date': date_begin,
            'attractions': attractions,
            'page_name': 'Attraction',
            'pager': pager,
            'default_url': '/my/attractions',
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'filterby': filterby,
        })
        return request.render("dti.portal_my_attraction_list", values)

    @http.route(['/my/attractions/<int:attraction_id>'], type='http',
                auth="public", website=True)
    def portal_my_attraction_detail(self, attraction_id, access_token=None,
                                    report_type=None, download=False, **kw):
        try:
            attraction_sudo = self._document_check_access('dti.attraction',
                                                          attraction_id,
                                                          access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        values = self._attraction_get_page_view_values(attraction_sudo,
                                                       access_token, **kw)

        attraction_type_list = []
        state_list = []
        identification_type_list = []

        state_list = attraction_sudo.state_id.search([
            ('country_id.name', 'ilike', 'argentina')])

        attraction_type_list = attraction_sudo.attraction_type_id.search([])

        identification_type_list = http.request.env[
            'l10n_latam.identification.type'].sudo().search(
                [('active', '=', True)])

        values.update({'state_list': state_list,
                       'attraction_type_list': attraction_type_list,
                       'identification_type_list': identification_type_list})

        return request.render("dti.attraction_page_template_edit", values)

    @http.route(['/my/attractions/delete/<int:attraction_id>'], type='http',
                auth="public", website=True)
    def portal_my_attraction_delete(self, attraction_id, access_token=None,
                                    report_type=None, download=False, **kw):
        try:
            attraction_sudo = self._document_check_access('dti.attraction',
                                                          attraction_id,
                                                          access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        attraction_sudo.unlink()

        return request.redirect('/my/attractions')
