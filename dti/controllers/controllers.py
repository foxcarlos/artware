
from odoo import http


class DtiMunicipalityController(http.Controller):

    def _know_entity_from_partner(self, partner):
        """from the partner I can know to which entity it belongs"""

        entity, entity_id = '', ''

        municipality = http.request.env['dti.municipality']
        municipality_id = municipality.sudo().search([
            ('representative_id', '=', partner.id)])

        business = http.request.env['dti.business']
        business_id = business.sudo().search([
            ('representative_id', '=', partner.id)])

        institution = http.request.env['dti.institution']
        institution_id = institution.sudo().search([
            ('representative_id', '=', partner.id)])

        person = http.request.env['dti.person']
        person_id = person.sudo().search([
            ('partner_id', '=', partner.id)])

        if municipality_id:
            entity = municipality
            entity_id = municipality_id
        elif business_id:
            entity = business
            entity_id = business_id
        elif institution_id:
            entity = institution
            entity_id = institution_id
        elif person_id:
            entity = person
            entity_id = person_id

        return entity, entity_id

    def _render_municipality(self, metodo, entity, entity_id, data):

        values = {}
        state_list = []
        types_area = []
        type_area = ''
        interventions_list = []
        identification_type_list = []

        if metodo == 'GET':
            state_list = entity.state_id.sudo().search([
                ('country_id.name', 'ilike', 'argentina')], order="name")

            types_area = [area[1] for area in entity.AREA]
            for _area in entity.AREA:
                if _area[0] == entity_id.type_area:
                    type_area = _area[1]

            interventions_list = http.request.env['dti.intervention'].sudo(
                ).search([('active', '=', True)], order="name")

            identification_type_list = http.request.env[
                'l10n_latam.identification.type'].sudo().search([
                    ('active', '=', True)])

            values = {'municipality': entity_id,
                      'state_list': state_list,
                      'types_area': types_area,
                      'type_area': type_area,
                      'interventions_list': interventions_list,
                      'identification_type_list': identification_type_list}

            return 'dti.municipality_page_template_edit', {'values': values,
                                                           'page_name':
                                                           'Municipality'}

        if metodo == 'POST':
            intervention_ids_form = http.request.httprequest.form.getlist(
                'intervention_ids')

            data['intervention_ids'] = intervention_ids_form
            data.update({'municipality_id': entity_id.id})
            entity.sudo().update_municipality(data)
            return 'dti.municipality_page_post_update_template', {
                'msg': 'Miembros Municipios'}

        return False

    def _render_business(self, metodo, entity, entity_id, data):
        values = {}
        state_list = []
        business_subtypes = []
        business_types = []
        identification_type_list = []

        if metodo == 'GET':
            state_list = entity.state_id.sudo().search([
                ('country_id.name', 'ilike', 'argentina')], order="name")

            business_subtypes = http.request.env[
                'dti.business.subtype'].search([('active', '=', True)],
                                               order="name")

            business_types = http.request.env['dti.business.type'].search([
                ('active', '=', True)], order="name")

            identification_type_list = http.request.env[
                'l10n_latam.identification.type'].sudo().search([
                    ('active', '=', True)])

            interventions_list = http.request.env['dti.intervention'].sudo(
                ).search([('active', '=', True)], order="name")

            values = {'business': entity_id,
                      'state_list': state_list,
                      'business_type': business_types,
                      'business_subtype': business_subtypes,
                      'identification_type_list': identification_type_list,
                      'interventions_list': interventions_list}

            return 'dti.business_page_template_edit', {'values': values,
                                                       'page_name': 'Business'}

        if metodo == 'POST':
            business_type = http.request.httprequest.form.getlist(
                'business_type')

            business_subtype = http.request.httprequest.form.getlist(
                'business_subtype')

            intervention_ids_form = http.request.httprequest.form.getlist(
                'intervention_ids')

            data['intervention_ids'] = intervention_ids_form
            data['business_type'] = business_type
            data['business_subtype'] = business_subtype
            data.update({'business_id': entity_id.id})

            entity.sudo().update_business(data)
            return 'dti.municipality_page_post_update_template', {
                'msg': 'Miembros Colaboradores'}
        return False

    def _render_institution(self, metodo, entity, entity_id, data):
        values = {}
        state_list = []
        institution_types = []
        identification_type_list = []

        if metodo == 'GET':
            state_list = entity.state_id.sudo().search([
                ('country_id.name', 'ilike', 'argentina')], order="name")

            attraction_list = http.request.env['dti.attraction'].sudo().search(
                [('active', '=', True)], order="name")

            institution_types = http.request.env['dti.institution.type'].sudo(
                ).search([('active', '=', True)], order="name")

            identification_type_list = http.request.env[
                'l10n_latam.identification.type'].sudo().search(
                    [('active', '=', True)])

            interventions_list = http.request.env['dti.intervention'].sudo(
                ).search([('active', '=', True)], order="name")

            values = {'institution': entity_id,
                      'state_list': state_list,
                      'attraction_ids': attraction_list,
                      'institution_type': institution_types,
                      'identification_type_list': identification_type_list,
                      'interventions_list': interventions_list}

            return 'dti.institution_page_template_edit', {'values': values,
                                                          'page_name':
                                                          'Institution'}

        if metodo == 'POST':
            institution_type = http.request.httprequest.form.getlist(
                'institution_type')

            intervention_ids_form = http.request.httprequest.form.getlist(
                'intervention_ids')

            data['intervention_ids'] = intervention_ids_form
            data['institution_type'] = institution_type
            data.update({'institution_id': entity_id.id})

            entity.sudo().update_institution(data)
            return 'dti.municipality_page_post_update_template', {
                'msg': 'Miembros Institucion'}
        return False

    def _render_person(self, metodo, entity, entity_id, data):
        values = {}
        state_list = []
        identification_type_list = []

        if metodo == 'GET':
            state_list = entity.state_id.sudo().search([
                ('country_id.name', 'ilike', 'argentina')], order="name")

            education_list = entity.education.search([('active', '=', True)])

            identification_type_list = http.request.env[
                'l10n_latam.identification.type'].sudo().search([
                    ('active', '=', True)])

            interventions_list = http.request.env['dti.intervention'].sudo(
                ).search([('active', '=', True)], order="name")

            sector_list = http.request.env['dti.sector'].sudo().search([
                ('active', '=', True)], order="name")

            values = {'person': entity_id,
                      'state_list': state_list,
                      'education_list': education_list,
                      'sector_list': sector_list,
                      'identification_type_list': identification_type_list,
                      'interventions_list': interventions_list
                      }

            return 'dti.person_page_template_edit', {'values': values,
                                                     'page_name': 'Person'}

        if metodo == 'POST':
            sector_id = http.request.httprequest.form.getlist(
                'sector_id')

            intervention_ids_form = http.request.httprequest.form.getlist(
                'intervention_ids')

            data['intervention_ids'] = intervention_ids_form
            data['sector_id'] = sector_id
            data.update({'person_id': entity_id.id})

            entity.sudo().update_person(data)
            return 'dti.municipality_page_post_update_template', {
                'msg': 'Miembros Persona'}
        return False

    @http.route(['/cuenta'],  type='http', auth='user', website=True,
                methods=['GET', 'POST'])
    def dti_mi_cuenta(self, **kw):

        data = kw
        metodo = http.request.httprequest.method
        partner = http.request.env.user.partner_id

        if not partner:
            msg1 = """No se encontro ningun contacto o representante asociado
            a este ente"""
            msg2 = "Contacte con el administrador del sistema"

            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})

        entity, entity_id = self._know_entity_from_partner(partner)

        if not entity_id:
            msg1 = """Este contacto no esta asociado a ningun ente"""
            msg2 = "Contacte con el administrador del sistema"

            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})

        if entity._name == 'dti.municipality':
            template, values = self._render_municipality(metodo, entity,
                                                         entity_id, data)
            return http.request.render(template, values)

        if entity._name == 'dti.business':
            template, values = self._render_business(metodo, entity,
                                                     entity_id, data)
            return http.request.render(template, values)

        if entity._name == 'dti.institution':
            template, values = self._render_institution(metodo, entity,
                                                        entity_id, data)
            return http.request.render(template, values)

        if entity._name == 'dti.person':
            template, values = self._render_person(metodo, entity,
                                                   entity_id, data)
            return http.request.render(template, values)
        return False

    @http.route(['/miembros-municipios'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_municipality_get(self, **kw):
        data = kw

        state_list = []
        types_area = []
        interventions_list = []
        identification_type_list = []

        municipality = http.request.env['dti.municipality']
        metodo = http.request.httprequest.method

        if metodo == 'GET':
            state_list = municipality.state_id.sudo().search([
                ('country_id.name', 'ilike', 'argentina')], order="name")

            types_area = [area[1] for area in municipality.AREA]

            interventions_list = http.request.env['dti.intervention'].search(
                [('active', '=', True)], order="name")

            identification_type_list = http.request.env[
                'l10n_latam.identification.type'].sudo().search([
                    ('active', '=', True)])

        elif metodo == 'POST':
            municipality.sudo().create_municipality(data)
            msg1 = 'Miembros Municipios'
            msg2 = ''
            return http.request.render('dti.municipality_page_post_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})

        return http.request.render('dti.municipality_page_template', {
            'state_list': state_list,
            'types_area': types_area,
            'interventions_list': interventions_list,
            'identification_type_list': identification_type_list})

    @http.route(['/miembros-municipios-post'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_municipality_post(self, **kw):
        return http.request.render('dti.test_page_template',
                                   {'test': []})

    @http.route(['/pedir-baja'], type='http', auth='public',
                website=True, csrf=True, methods=['GET', 'POST'])
    def dti_pedir_baja(self, **kw):
        data = kw
        partner = http.request.env.user.partner_id
        entity = partner.know_entity_from_partner()
        metodo = http.request.httprequest.method
        _, entity_id = self._know_entity_from_partner(partner)

        if not entity_id:
            msg1 = """Este contacto no esta asociado a ningun ente"""
            msg2 = "Contacte con el administrador del sistema"

            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})

        if metodo == 'GET':
            return http.request.render('dti.unsuscribe_page_template',
                                       {'values': entity,
                                        'page_name': 'Unsuscribe'})

        if metodo == 'POST':
            if data.get('pedirBaja') == 'on':
                motivo = data.get('reason_unsubscribe')
                entity.unsubscribe = True
                entity.reason_unsubscribe = motivo
                entity.active = False
                entity.contact_id = None
                entity.representative_id = None
                entity._cr.commit()

                msg2 = "Ha sido dado de baja satisfactoriamente"
                msg1 = "Entidad: {} ".format(entity.name)

                return http.request.render('dti.messageok_page_template',
                                           {'msg1': msg1, 'msg2': msg2})
        return False

    """
    @http.route(['/protocolo-de-adhesion'], type='http', auth='public',
                website=True, csrf=False, methods=['GET'])
    def dti_protocolo_de_adhesion(self, **kw):
        return http.request.render('dti.protocolo_page_template', {'val': []})

    @http.route(['/politica-de-privacidad'], type='http', auth='public',
                website=True, csrf=False, methods=['GET'])
    def dti_politicas_de_privacidad(self, **kw):
        return http.request.render('dti.politica_page_template', {'val': []})
    """
