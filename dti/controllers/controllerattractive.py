from odoo import http


class DtiAttractiveController(http.Controller):

    def _attractive_is_municipality_or_institution(self):
        logger = http.request.env.user.partner_id.know_entity_from_partner()
        if logger:
            if logger._name == 'dti.municipality' or \
                    logger._name == 'dti.institution':
                return logger
        return False

    @http.route(['/miembros-atractivos'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_attractive_get(self, **kw):
        data = kw

        logger = self._attractive_is_municipality_or_institution()
        if not logger:
            msg1 = """Solo los Municipios e instituciones pueden crear o editar
            atractivoa turisticoa, si considera que esto es un error contacte
            con el Administrador del sistema"""
            msg2 = ''
            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values': {'msg1': msg1,
                                                   'msg2': msg2}})

        attraction_type_list = []
        state_list = []
        identification_type_list = []

        attraction = http.request.env['dti.attraction']
        metodo = http.request.httprequest.method

        state_list = attraction.state_id.search([
            ('country_id.name', 'ilike', 'argentina')])

        attraction_type_list = http.request.env[
            'dti.attraction.type'].sudo().search([
                ('active', '=', True)], order="name")

        identification_type_list = http.request.env[
            'l10n_latam.identification.type'].sudo().search([
                ('active', '=', True)])

        if metodo == 'GET':
            return http.request.render('dti.attraction_page_template', {
                'state_list': state_list,
                'attraction_type_list': attraction_type_list,
                'page_name': 'Attraction',
                'identification_type_list': identification_type_list})

        elif metodo == 'POST':
            data.update({'entity': logger})
            response = attraction.sudo().create_attraction(data)

            if not response[0]:
                msg1 = """No se pudo crear el atractivo turistico,
                reporte al administrador"""
                msg2 = response[1]
                return http.request.render('dti.warning_sin_contacto_template',
                                           {'values': {'msg1': msg1,
                                                       'msg2': msg2}
                                            })
            msg1 = 'Miembros atractivos turisticos'
            msg2 = ''
            return http.request.render(
                'dti.municipality_page_post_template',
                {'values': {'msg1': msg1, 'msg2': msg2}})
        else:
            msg1 = 'Solicitud no puede ser procesada.'
            msg2 = 'Reporte el caso al administrador de sistemas: '
            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})

    @http.route(['/miembros-atractivos-edit'], type='http',
                auth='public', website=True, csrf=False,
                methods=['GET', 'POST'])
    def dti_attractive_edit(self, **kw):
        http.request.env['dti.attraction'].sudo().update_attraction(kw)
        return http.request.redirect('/my/attractions')
