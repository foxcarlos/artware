
from odoo import http


class DtiBusinessController(http.Controller):

    @http.route(['/miembros-empresas'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_business_get(self, **kw):
        data = kw

        state_list = []
        business_types = []
        business_subtypes = []
        identification_type_list = []

        business = http.request.env['dti.business']
        metodo = http.request.httprequest.method
        state_list = business.state_id.search([
            ('country_id.name', 'ilike', 'argentina')], order="name")

        business_subtypes = http.request.env['dti.business.subtype'].search([
            ('active', '=', True)], order="name")

        business_types = http.request.env['dti.business.type'].search([
            ('active', '=', True)], order="name")

        identification_type_list = http.request.env[
            'l10n_latam.identification.type'].search([
                ('active', '=', True)])

        if metodo == 'GET':
            return http.request.render('dti.business_page_template', {
                'state_list': state_list,
                'business_types': business_types,
                'business_subtypes': business_subtypes,
                'identification_type_list': identification_type_list})

        elif metodo == 'POST':
            business_type = http.request.httprequest.form.getlist(
                'business_type')
            business_subtype = http.request.httprequest.form.getlist(
                'business_subtype')

            data.update({'business_type': business_type})
            data.update({'business_subtype': business_subtype})

            response = business.sudo().create_business(data)
            if not response[0]:
                msg1 = 'No se pudo crear la Empresa, reporte al administrador'
                msg2 = response[1]
                return http.request.render('dti.warning_sin_contacto_template',
                                           {'values': {'msg1': msg1,
                                                       'msg2': msg2}
                                            })
            msg1 = 'Miembros Empresas'
            msg2 = ''
            return http.request.render(
                'dti.municipality_page_post_template',
                {'values': {'msg1': msg1, 'msg2': msg2}})
        else:
            msg1 = 'Solicitud no puede ser procesada.'
            msg2 = 'Reporte el caso al administrador de sistemas: '
            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})
