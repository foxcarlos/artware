
from odoo import http


class DtiPersonController(http.Controller):

    @http.route(['/miembros-personas'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_person_get(self, **kw):
        data = kw

        state_list = []
        sector_list = []
        identification_type_list = []
        interventions_list = []

        person = http.request.env['dti.person']
        metodo = http.request.httprequest.method

        education_list = person.education.search([])

        state_list = person.state_id.search([
            ('country_id.name', 'ilike', 'argentina')], order="name")

        interventions_list = http.request.env[
            'dti.intervention'].sudo().search([
                ('active', '=', True)], order="name")

        sector_list = http.request.env['dti.sector'].sudo().search([
            ('active', '=', True)], order="name")

        identification_type_list = http.request.env[
            'l10n_latam.identification.type'].sudo().search([
                ('active', '=', True)])

        if metodo == 'GET':
            return http.request.render('dti.person_page_template', {
                'state_list': state_list,
                'education_list': education_list,
                'sector_list': sector_list,
                'interventions_list': interventions_list,
                'identification_type_list': identification_type_list})

        elif metodo == 'POST':
            sector_ids = http.request.httprequest.form.getlist(
                'sector_id')

            intervention_ids = http.request.httprequest.form.getlist(
                'intervention_ids')

            education_ids = http.request.httprequest.form.getlist(
                'education_id')

            data['intervention_ids'] = intervention_ids
            data['education_id'] = education_ids
            data['sector_id'] = sector_ids

            response = person.sudo().create_person(data)

            if not response[0]:
                msg1 = """No se pudo crear la persona,
                reporte al administrador"""
                msg2 = response[1]
                return http.request.render('dti.warning_sin_contacto_template',
                                           {'values': {'msg1': msg1,
                                                       'msg2': msg2}
                                            })
            msg1 = 'Miembros Persona'
            msg2 = ''
            return http.request.render(
                'dti.municipality_page_post_template',
                {'values': {'msg1': msg1, 'msg2': msg2}})
        else:
            msg1 = 'Solicitud no puede ser procesada.'
            msg2 = 'Reporte el caso al administrador de sistemas: '
            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})
