
from odoo import http


class DtiInstitutionController(http.Controller):

    @http.route(['/miembros-institucion'], type='http', auth='public',
                website=True, csrf=False, methods=['GET', 'POST'])
    def dti_institution_get(self, **kw):
        data = kw

        state_list = []
        institution_types = []
        identification_type_list = []

        institution = http.request.env['dti.institution']
        metodo = http.request.httprequest.method
        state_list = institution.state_id.search([
            ('country_id.name', 'ilike', 'argentina')], order="name")

        institution_types = http.request.env['dti.institution.type'].search([
            ('active', '=', True)], order="name")

        attraction_list = http.request.env['dti.attraction'].search([
            ('active', '=', True)], order="name")

        identification_type_list = http.request.env[
            'l10n_latam.identification.type'].search([
                ('active', '=', True)])

        if metodo == 'GET':
            return http.request.render('dti.institution_page_template', {
                'is_tourist_attraction': False,
                'attraction_ids': attraction_list,
                'state_list': state_list,
                'institution_types': institution_types,
                'identification_type_list': identification_type_list})

        elif metodo == 'POST':
            institution_type = http.request.httprequest.form.getlist(
                'institution_type')
            data.update({'institution_type': institution_type})

            response = institution.sudo().create_institution(data)
            if not response[0]:
                msg1 = """No se pudo crear la Institucion,
                reporte al administrador"""
                msg2 = response[1]
                return http.request.render('dti.warning_sin_contacto_template',
                                           {'values': {'msg1': msg1,
                                                       'msg2': msg2}
                                            })
            msg1 = 'Miembros Institucion'
            msg2 = ''
            return http.request.render(
                'dti.municipality_page_post_template',
                {'values': {'msg1': msg1, 'msg2': msg2}})
        else:
            msg1 = 'Solicitud no puede ser procesada.'
            msg2 = 'Reporte el caso al administrador de sistemas: '
            return http.request.render('dti.warning_sin_contacto_template',
                                       {'values':
                                        {'msg1': msg1, 'msg2': msg2}})
