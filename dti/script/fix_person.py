# pylint: disable=broad-except

import csv


def parse_data(data):
    names = ''
    lastnames = ''
    identification_type_name = ''
    identification_type = ''
    vat = ''
    birthday = ''
    street = ''
    country = ''
    state_name = ''
    state_id = ''
    city = ''
    mobile = ''
    email = ''
    sector_name = ''
    sector_id = ''
    education = ''
    education_id = ''
    twitter = ''
    facebook = ''
    instagram = ''
    linkedin = ''
    intervention_name = ''
    adt_ids = ''
    adherence_protocol = ''
    privavy_policies = ''

    rp = env['res.partner']
    rp.l10n_latam_identification_type_id.search([])

    _id, names, lastnames, identification_type_name, vat,\
        birthday, street,  country, state_name, state_id, city, mobile, \
        email, sector_name, sector_id, education, education_id, twitter, \
        facebook, instagram, linkedin, intervention_name, adt_ids, \
        adherence_protocol, privavy_policies = data

    record = {}
    record = {'name': names,
              'lastname': lastnames,
              'identification_type': identification_type,
              'vat': vat,
              'birthday': birthday,
              'street': street,
              'state_id': state_id,
              'city': city,
              'mobile': mobile,
              'email': email,
              'sector_id': [sector_name],
              'education_id': [education_id] if education_id else '',
              'twitter': twitter,
              'facebook': facebook,
              'instagram': instagram,
              'linkedin': linkedin,
              'intervention_ids': [intervention_name],
              'adherence_protocol': adherence_protocol,
              'privavy_policies': privavy_policies}
    return record


def main():

    with open('dti.person.csv', newline='') as csvfile:

        spamreader = csv.reader(csvfile)
        for index, row in enumerate(spamreader):
            if index == 0:
                continue
            print(', '.join(row))
