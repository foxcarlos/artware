# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except

import base64
import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    lastname = fields.Char()
    municipality_ids = fields.One2many('dti.municipality', 'partner_id',
                                       string='Municipalities', auto_join=True)
    business_ids = fields.One2many('dti.business', 'partner_id',
                                   string='Business', auto_join=True)
    institution_ids = fields.One2many('dti.institution', 'partner_id',
                                      string='Institutions', auto_join=True)
    person_ids = fields.One2many('dti.person', 'partner_id',
                                 string='Persons', auto_join=True)
    attraction_ids = fields.One2many('dti.attraction', 'partner_id',
                                     string='Attrations', auto_join=True)
    birthday = fields.Date()
    designation_doc = fields.Binary(string="Designation Document",
                                    attachment=True)
    intervention_ids = fields.Many2many('dti.intervention',
                                        string='Intervention Area')
    represents = fields.Char(compute='_who', string='It represents')

    def _who(self):
        for record in self:
            entity = self.know_entity_from_partner().name
            data = self.know_entity_from_partner()._name
            if data == 'dti.municipality':
                data = 'Municipalidad:{}'.format(entity)
            elif data == 'dti.institution':
                data = 'Institucion:{}'.format(entity)
            elif data == 'dti.business':
                data = 'Colaborador:{}'.format(entity)
            else:
                data = ''
            record.represents = data

    def update_representative(self, data):

        attachment = ''
        names = data.get('representative_id_names')
        lastnames = data.get('representative_id_lastnames')
        identification_type = data.get('identification_type2')
        vat = data.get('representative_id_vat')
        birthday = data.get('representative_id_birthday')
        function = data.get('representative_id_function')
        email = data.get('representative_id_email')
        mobile = data.get('representative_id_mobile')
        phone = data.get('representative_id_phone')
        contact_is_equal_representative = True if data.get('customSwitch1') \
            == 'on' else False
        intervention_id = data.get('intervention_ids')

        if data.get('representative_id_designation_doc', False):
            attachment = base64.encodestring(
                data.get('representative_id_designation_doc').read())
        elif data.get('mostrar_designation_doc', False):
            attachment = data.get('mostrar_designation_doc')

        values = {'name': names,
                  'lastname': lastnames,
                  'l10n_latam_identification_type_id': int(
                      identification_type),
                  'vat': vat,
                  'birthday': birthday or None,
                  'function': function,
                  'email': email,
                  'mobile': mobile,
                  'phone': phone,
                  'designation_doc': attachment}

        adt_ids = self.env['dti.intervention'].search([
            ('name', 'in', intervention_id)])
        if contact_is_equal_representative:
            values.update({'intervention_ids': adt_ids or None})

        try:
            self.write(values)
        except Exception as error_update:
            _logger.error('error_update', error_update=error_update)
            return False

        return self.id

    def update_contact(self, data):

        names = data.get('contac_id_names')
        lastnames = data.get('contac_id_lastnames')
        identification_type = data.get('identification_type')
        vat = data.get('contac_id_vat')
        birthday = data.get('contac_id_birthday')
        function = data.get('contac_id_function')
        email = data.get('contac_id_email')
        mobile = data.get('contac_id_mobile')
        intervention_id = data.get('intervention_ids')
        adt_ids = self.env['dti.intervention'].search([
            ('name', 'in', intervention_id)])

        values = {'name': names,
                  'lastname': lastnames,
                  'l10n_latam_identification_type_id': int(
                      identification_type),
                  'vat': vat,
                  'birthday': birthday or None,
                  'function': function,
                  'intervention_ids': adt_ids if adt_ids else None,
                  'email': email,
                  'mobile': mobile}

        try:
            self.write(values)
        except Exception as error_write:
            _logger.error('error_write', error_write=error_write)
            return False

        return self.id

    def create_representative(self, data):
        res_partner = self.env['res.partner']

        names = data.get('representative_id_names')
        lastnames = data.get('representative_id_lastnames')
        function = data.get('representative_id_function')
        email = data.get('representative_id_email')
        mobile = data.get('representative_id_mobile')

        try:
            res_id = res_partner.create({
                'name': names,
                'lastname': lastnames,
                'function': function,
                'email': email,
                'mobile': mobile})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False

        return res_id

    def create_contact(self, data):
        res_partner = self.env['res.partner']

        names = data.get('contac_id_names')
        lastnames = data.get('contac_id_lastnames')
        function = data.get('contac_id_function')
        email = data.get('contac_id_email')
        mobile = data.get('contac_id_mobile')

        try:
            res_id = res_partner.create({
                'name': names,
                'lastname': lastnames,
                'function': function,
                'email': email,
                'mobile': mobile})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False

        return res_id

    def search_blog_post(self):
        limit = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'dti.my.blogposts')])
        entity = self.know_entity_from_partner()
        try:
            tag_ids = entity.tag_ids.ids
        except Exception as error_tag_ids:
            _logger.error('error_tag_ids', error_tag_ids=error_tag_ids)
            tag_ids = []

        domain = [('tag_ids', 'in', tag_ids)]
        blog_post = self.env['blog.post'].search(domain,
                                                 limit=int(limit.value) or 6)
        return blog_post

    def know_entity_from_partner(self):
        """from the partner I can know to which entity it belongs"""

        entity_id = ''

        municipality = self.env['dti.municipality']
        municipality_id = municipality.sudo().search([
            ('representative_id', '=', self.id)])

        business = self.env['dti.business']
        business_id = business.sudo().search([
            ('representative_id', '=', self.id)])

        institution = self.env['dti.institution']
        institution_id = institution.sudo().search([
            ('representative_id', '=', self.id)])

        person = self.env['dti.person']
        person_id = person.sudo().search([
            ('partner_id', '=', self.id)])

        if municipality_id:
            entity_id = municipality_id
        elif business_id:
            entity_id = business_id
        elif institution_id:
            entity_id = institution_id
        elif person_id:
            entity_id = person_id
        else:
            for record in self:
                entity_id = record

        return entity_id
