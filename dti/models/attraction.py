# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except

import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class DtiAttractionType(models.Model):
    _name = 'dti.attraction.type'

    name = fields.Char()
    description = fields.Char()
    active = fields.Boolean(default=True)


class DtiAttraction(models.Model):
    _name = 'dti.attraction'
    _inherits = {'res.partner': 'partner_id'}
    partner_id = fields.Many2one('res.partner')

    attraction_type_id = fields.Many2one('dti.attraction.type')
    municipality_id = fields.Many2one('dti.municipality')
    institution_id = fields.Many2one('dti.institution')
    latitude = fields.Char()
    longitude = fields.Char()
    active = fields.Boolean(default=True)

    def create_attraction(self, data):

        name = data.get('name')
        street = data.get('street')
        state_id = data.get('state_id')
        city = data.get('city')
        phone = data.get('phone')
        mobile = data.get('mobile')
        attraction_type_id = data.get('attraction_type_id')
        latitude = data.get('latitud')
        longitude = data.get('longitud')
        entity = data.get('entity')

        municipality_id = entity.id if \
            entity._name == 'dti.municipality' else None
        institution_id = entity.id if \
            entity._name == 'dti.institution' else None

        try:
            self.create({
                'name': name,
                'street': street,
                'state_id': state_id,
                'city': city,
                'phone': phone,
                'mobile': mobile,
                'attraction_type_id': attraction_type_id,
                'latitude': latitude,
                'longitude': longitude,
                'municipality_id': municipality_id,
                'institution_id': institution_id
            })
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False, error_create
        return True, ''

    def update_attraction(self, data):
        update = self.env['dti.attraction'].search(
            [('id', '=', data.get('id'))])
        name = data.get('name')
        street = data.get('street')
        state_id = data.get('state_id')
        city = data.get('city')
        phone = data.get('phone')
        mobile = data.get('mobile')
        attraction_type_id = data.get('attraction_type_id')
        latitude = data.get('latitude')
        longitude = data.get('longitude')

        try:
            update.write({
                'name': name,
                'street': street,
                'state_id': state_id,
                'city': city,
                'phone': phone,
                'mobile': mobile,
                'attraction_type_id': attraction_type_id,
                'latitude': latitude,
                'longitude': longitude,
            })
        except Exception as error_update:
            _logger.error('error_update', error_update=error_update)
            return False, error_update
        return True, ''
