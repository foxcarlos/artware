# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except

import base64
import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class DtiInterventionArea(models.Model):

    _name = 'dti.intervention'
    _description = 'Intervention Area'

    name = fields.Char()
    description = fields.Char()
    active = fields.Boolean(default=True)


class DtiMunicipality(models.Model):

    _name = 'dti.municipality'
    _inherits = {'res.partner': 'partner_id'}
    partner_id = fields.Many2one('res.partner')

    AREA = [
        ('AG', 'Agencia Turismo'),
        ('AA', 'Área Turismo'),
        ('DT', 'Direccion de turismo'),
        ('EE', 'Ente Turismo'),
        ('ST', 'Secretaría Turismo'),
        ('SST', 'SubSecretaría Turismo'),
        ('OT', 'Otro')]

    MEMBER_TYPE = [
        (1, 'MUNICIPALITY'),
        (2, 'INSTITUTE'),
        (3, 'COMPANY'),
        (4, 'PERSON')]

    partner_id = fields.Many2one('res.partner', required=True,
                                 ondelete='restrict', auto_join=True,
                                 string='Related Partner',
                                 help=('Partner-related data of the '
                                       'municipality'))
    intendant_name = fields.Char()
    intendant_lastname = fields.Char()
    type_area = fields.Selection(AREA)
    number_members = fields.Integer()
    other_type_area = fields.Char()
    twitter = fields.Char()
    facebook = fields.Char()
    instagram = fields.Char()
    linkedin = fields.Char()
    tag_ids = fields.Many2many('blog.tag', string='Blog Tag')
    unsubscribe = fields.Boolean()
    reason_unsubscribe = fields.Char()
    representative_id = fields.Many2one('res.partner',
                                        string=('representative of the'
                                                'municipality'))
    contact_id = fields.Many2one('res.partner',
                                 string='Contact municipality')
    attraction_ids = fields.One2many('dti.attraction', 'municipality_id',
                                     string='Attrations')
    adherence_protocol = fields.Boolean()
    adherence_version = fields.Char()
    privacy_policies = fields.Boolean()
    privacy_version = fields.Char()
    active = fields.Boolean(default=True)

    def update_representative(self, data):
        municipality = self.search([('id', '=', data.get('municipality_id'))])

        attachment = ''
        names = data.get('representative_id_names')
        lastnames = data.get('representative_id_lastnames')
        identification_type = data.get('identification_type2')
        vat = data.get('representative_id_vat')
        birthday = data.get('representative_id_birthday')
        function = data.get('representative_id_function')
        email = data.get('representative_id_email')
        mobile = data.get('representative_id_mobile')
        phone = data.get('representative_id_phone')
        contact_is_equal_representative = True if data.get('customSwitch1') \
            == 'on' else False
        intervention_id = data.get('intervention_ids')

        if data.get('representative_id_designation_doc', False):
            attachment = base64.encodestring(
                data.get('representative_id_designation_doc').read())
        elif data.get('mostrar_designation_doc', False):
            attachment = data.get('mostrar_designation_doc')

        values = {'name': names,
                  'lastname': lastnames,
                  'l10n_latam_identification_type_id': int(
                      identification_type),
                  'vat': vat,
                  'birthday': birthday or None,
                  'function': function,
                  'email': email,
                  'mobile': mobile,
                  'phone': phone,
                  'designation_doc': attachment}

        if contact_is_equal_representative:
            values.update({'intervention_ids': intervention_id or None})

        if municipality.representative_id:
            try:
                municipality.representative_id.write(values)
            except Exception as error_update:
                _logger.error('error_update', error_update=error_update)
                return False
        else:
            try:
                _id = municipality.representative_id.create(values)
                municipality.write({'representative_id': _id})
                return _id
            except Exception as error_create:
                _logger.error('error_create', error_create=error_create)
                return False

        return municipality.representative_id

    def update_contact(self, data):
        municipality = self.search([('id', '=', data.get('municipality_id'))])

        names = data.get('contac_id_names')
        lastnames = data.get('contac_id_lastnames')
        identification_type = data.get('identification_type')
        vat = data.get('contac_id_vat')
        birthday = data.get('contac_id_birthday')
        function = data.get('contac_id_function')
        email = data.get('contac_id_email')
        mobile = data.get('contac_id_mobile')
        intervention_id = data.get('intervention_ids')

        values = {'name': names,
                  'lastname': lastnames,
                  'l10n_latam_identification_type_id': int(
                      identification_type),
                  'vat': vat,
                  'birthday': birthday or None,
                  'function': function,
                  'intervention_ids': intervention_id or None,
                  'email': email,
                  'mobile': mobile}

        if municipality.contact_id:
            try:
                municipality.contact_id.write(values)
            except Exception as error_write:
                _logger.error('error_write', error_write=error_write)
                return False
        else:
            try:
                _id = municipality.contact_id.create(values)
                municipality.write({'contact_id': _id})
                return _id
            except Exception as error_create:
                _logger.error('error_create', error_create=error_create)
                return False

        return municipality.contact_id

    def update_municipality(self, data):
        municipality = self.search([('id', '=', data.get('municipality_id'))])

        image = ''

        if data.get('customSwitch1') == 'on':
            representative_id = self.update_representative(data)
            municipality.write({'contact_id': representative_id})
        else:
            self.update_representative(data)
            self.update_contact(data)

        name = data.get('name')
        vat = data.get('vat')
        intendant_lastname = data.get('intendant_lastname')
        intendant_name = data.get('intendant_name')
        type_area = [a[0] for a in self.AREA if a[1] == data.get(
            'type_area')][0]
        number_members = data.get('number_members')
        street = data.get('street')
        _zip = data.get('zip')
        city = data.get('city')
        state_id = data.get('state_id')
        phone = data.get('phone')
        website = data.get('website')
        twitter = data.get('twitter')
        facebook = data.get('facebook')
        instagram = data.get('instagram')
        linkedin = data.get('linkedin')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        if data.get('image_1920', False):
            image = base64.encodestring(data.get('image_1920').read())
        elif data.get('mostrar_logo', False):
            image = data.get('mostrar_logo')

        try:
            municipality.write({
                'name': name,
                'vat': vat,
                'intendant_name': intendant_name,
                'intendant_lastname': intendant_lastname,
                'type_area': type_area,
                'number_members': number_members,
                'street': street,
                'zip': _zip,
                'city': city,
                'state_id': state_id,
                'phone': phone,
                'website': website,
                'twitter': twitter,
                'facebook': facebook,
                'instagram': instagram,
                'linkedin': linkedin,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version,
                'image_1920': image})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)

            return False
        return True

    def create_municipality(self, data):

        res_partner = self.env['res.partner']

        if data.get('customSwitch1') == 'on':
            representative_id = res_partner.create_representative(data).id
            contact_id = representative_id
        else:
            representative_id = res_partner.create_representative(data).id
            contact_id = res_partner.create_contact(data).id

        name = data.get('name')
        intendant_lastname = data.get('intendant_lastname')
        intendant_name = data.get('intendant_name')
        type_area = [a[0] for a in self.AREA if a[1] == data.get(
            'type_area')][0]
        street = data.get('street')
        city = data.get('city')
        state_id = data.get('state_id')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        try:
            self.create({
                'name': name,
                'intendant_name': intendant_name,
                'intendant_lastname': intendant_lastname,
                'type_area': type_area,
                'street': street,
                'city': city,
                'state_id': state_id,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version,
                'representative_id': representative_id,
                'contact_id': contact_id})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)

            return False
        return True
