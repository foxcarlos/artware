# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except
# pylint: disable=redefined-builtin

import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class DtiInstitution(models.Model):
    _name = 'dti.institution'
    _inherits = {'res.partner': 'partner_id'}
    partner_id = fields.Many2one('res.partner')

    partner_id = fields.Many2one('res.partner', required=True,
                                 ondelete='restrict', auto_join=True,
                                 string='Related Partner',
                                 help=('Partner-related data of the '
                                       'institutions'))

    fantasy_name = fields.Char()
    institution_type = fields.Many2one('dti.institution.type')
    other_institution = fields.Char()
    is_tourist_attraction = fields.Boolean()
    attraction_id = fields.Many2one('dti.attraction')
    municipality_id = fields.Many2one('dti.municipality')
    attraction_ids = fields.One2many('dti.attraction', 'institution_id',
                                     string='Attrations')
    unsubscribe = fields.Boolean()
    reason_unsubscribe = fields.Char()
    tag_ids = fields.Many2many('blog.tag', string='Blog Tag')
    representative_id = fields.Many2one('res.partner',
                                        string=('representative of the'
                                                'municipality'))
    contact_id = fields.Many2one('res.partner', string='Contact institution')
    adherence_protocol = fields.Boolean()
    adherence_version = fields.Char()
    privacy_policies = fields.Boolean()
    privacy_version = fields.Char()
    active = fields.Boolean(default=True)

    def update_institution(self, data):
        institution = self.search([('id', '=', data.get('institution_id'))])

        # Si contacto es el mismo que el anterior
        if data.get('customSwitch1') == 'on':
            if institution.representative_id:  # Si tiene representante creado
                representative_id = \
                    institution.representative_id.update_representative(data)
                institution.write({'contact_id': representative_id})
            else:  # Si no tiene representante creado se tiene que crear
                representative_id = \
                    institution.representative_id.create_representative(data)
                institution.write({'contact_id': representative_id})
        else:  # Si el contacto no es el mismo que el antrerior
            if institution.representative_id:  # Si tiene representante creado
                representative_id = \
                    institution.representative_id.update_representative(data)
            else:
                representative_id = \
                    institution.representative_id.create_representative(data)
                institution.write({'representative_id': representative_id})

            if institution.contact_id:
                contact_id = institution.contact_id.update_contact(data)
            else:
                contact_id = institution.contact_id.create_contact(data)
                institution.write({'contact_id': contact_id})

        name = data.get('name')
        fantasy_name = data.get('fantasy_name')
        cuit = data.get('vat')
        institution_type = data.get('institution_type')
        other_institution = data.get('other_institution_type')
        is_tourist = data.get('is_tourist_attraction')
        attraction_id = data.get('attraction_id')
        street = data.get('street')
        city = data.get('city')
        _zip = data.get('zip')
        state_id = data.get('state_id')
        phone = data.get('phone')
        mobile = data.get('mobile')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        bt_ids = self.env['dti.institution.type'].search(
            [('name', 'in', institution_type)])
        attraction_id = self.env['dti.attraction'].search(
            [('name', 'ilike', attraction_id)])
        is_tourist_attraction = True if is_tourist == 'on' else False

        try:
            institution.write({
                'name': name,
                'fantasy_name': fantasy_name,
                'vat': cuit,
                'institution_type': bt_ids.ids if bt_ids else None,
                'other_institution': other_institution,
                'is_tourist_attraction': is_tourist_attraction,
                'attraction_id': attraction_id.id or None,
                'street': street,
                'zip': _zip,
                'city': city,
                'state_id': state_id,
                'phone': phone,
                'mobile': mobile,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False
        return True

    def create_institution(self, data):

        res_partner = self.env['res.partner']

        if data.get('customSwitch1') == 'on':
            representative_id = res_partner.create_representative(data).id
            contact_id = representative_id
        else:
            representative_id = res_partner.create_representative(data).id
            contact_id = res_partner.create_contact(data).id

        name = data.get('name')
        fantasy_name = data.get('fantasy_name')
        cuit = data.get('vat')
        institution_type = data.get('institution_type')
        other_institution = data.get('other_institution_type')
        is_tourist = data.get('is_tourist_attraction')
        attraction_id = data.get('attraction_id')
        street = data.get('street')
        city = data.get('city')
        zip = data.get('zip')
        state_id = data.get('state_id')
        phone = data.get('phone')
        mobile = data.get('mobile')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        bt_ids = self.env['dti.institution.type'].search(
            [('name', 'in', institution_type)])
        attraction_id = self.env['dti.attraction'].search(
            [('name', 'ilike', attraction_id)])
        is_tourist_attraction = True if is_tourist == 'on' else False

        try:
            self.create({
                'name': name,
                'fantasy_name': fantasy_name,
                'vat': cuit,
                'institution_type': bt_ids.id if bt_ids else None,
                'other_institution': other_institution,
                'is_tourist_attraction': is_tourist_attraction,
                'attraction_id': attraction_id.id or None,
                'street': street,
                'city': city,
                'zip': zip,
                'state_id': state_id,
                'phone': phone,
                'mobile': mobile,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version,
                'representative_id': representative_id,
                'contact_id': contact_id})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False, error_create
        return True, ''


class DtiInstitutionType(models.Model):
    _name = 'dti.institution.type'
    _description = 'Institution type'

    name = fields.Char()
    description = fields.Char()
    active = fields.Boolean(default=True)
