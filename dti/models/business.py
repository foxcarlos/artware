# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except
# pylint: disable=redefined-builtin


import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class DtiBusiness(models.Model):
    _name = 'dti.business'
    _inherits = {'res.partner': 'partner_id'}
    partner_id = fields.Many2one('res.partner')

    partner_id = fields.Many2one('res.partner', required=True,
                                 ondelete='restrict', auto_join=True,
                                 string='Related Partner',
                                 help=('Partner-related data of the '
                                       'business'))

    fantasy_name = fields.Char(required=True)
    business_type = fields.Many2many('dti.business.type')
    business_subtype = fields.Many2many('dti.business.subtype')
    other_business = fields.Char()
    representative_id = fields.Many2one('res.partner',
                                        string=('representative of the'
                                                'business'))
    contact_id = fields.Many2one('res.partner', string='Contact business')
    adherence_protocol = fields.Boolean()
    adherence_version = fields.Char()
    privacy_policies = fields.Boolean()
    privacy_version = fields.Char()
    unsubscribe = fields.Boolean()
    reason_unsubscribe = fields.Char()
    tag_ids = fields.Many2many('blog.tag', string='Blog Tag')
    active = fields.Boolean(default=True)

    def update_business(self, data):
        business = self.search([('id', '=', data.get('business_id'))])

        # Si contacto es el mismo que el anterior
        if data.get('customSwitch1') == 'on':
            if business.representative_id:  # Si tiene un representante creado
                representative_id = \
                    business.representative_id.update_representative(data)
                business.write({'contact_id': representative_id})
            else:  # Si no tiene representante creado se tiene que crear
                representative_id = \
                    business.representative_id.create_representative(data)
                business.write({'contact_id': representative_id})
        else:  # Si el contacto no es el mismo que el antrerior
            if business.representative_id:  # Si tiene un representante creado
                representative_id = \
                    business.representative_id.update_representative(data)
            else:
                representative_id = \
                    business.representative_id.create_representative(data)
                business.write({'representative_id': representative_id})

            if business.contact_id:
                contact_id = business.contact_id.update_contact(data)
            else:
                contact_id = business.contact_id.create_contact(data)
                business.write({'contact_id': contact_id})

        name = data.get('name')
        fantasy_name = data.get('fantasy_name')
        cuit = data.get('vat')
        business_type = data.get('business_type')
        business_subtype = data.get('business_subtype')
        other_business = data.get('other_business_type')
        street = data.get('street')
        city = data.get('city')
        _zip = data.get('zip')
        state_id = data.get('state_id')
        phone = data.get('phone')
        mobile = data.get('mobile')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')
        bt_ids = self.env['dti.business.type'].search(
            [('name', 'in', business_type)])
        bst_ids = self.env['dti.business.subtype'].search(
            [('name', 'in', business_subtype)])

        try:
            business.write({
                'name': name,
                'fantasy_name': fantasy_name,
                'vat': cuit,
                'business_type': bt_ids.ids if bt_ids else None,
                'business_subtype': bst_ids.ids if bst_ids else None,
                'other_business': other_business,
                'street': street,
                'zip': _zip,
                'city': city,
                'state_id': state_id,
                'phone': phone,
                'mobile': mobile,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False
        return True

    def create_business(self, data):

        res_partner = self.env['res.partner']

        if data.get('customSwitch1') == 'on':
            representative_id = res_partner.create_representative(data).id
            contact_id = representative_id
        else:
            representative_id = res_partner.create_representative(data).id
            contact_id = res_partner.create_contact(data).id

        name = data.get('name')
        fantasy_name = data.get('fantasy_name')
        cuit = data.get('vat')
        business_type = data.get('business_type')
        business_subtype = data.get('business_subtype')
        other_business = data.get('other_business_type')
        street = data.get('street')
        city = data.get('city')
        zip = data.get('zip')
        state_id = data.get('state_id')
        phone = data.get('phone')
        mobile = data.get('mobile')
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        bt_ids = self.env['dti.business.type'].search(
            [('name', 'in', business_type)])

        try:
            self.create({
                'name': name,
                'fantasy_name': fantasy_name,
                'vat': cuit,
                'business_type': bt_ids.ids if bt_ids else None,
                'business_subtype': business_subtype,
                'other_business': other_business,
                'street': street,
                'city': city,
                'zip': zip,
                'state_id': state_id,
                'phone': phone,
                'mobile': mobile,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version,
                'representative_id': representative_id,
                'contact_id': contact_id})
        except Exception as error_create:
            _logger.error('error_create', error_create=error_create)
            return False, error_create
        return True, ''


class DtiBusinessSubType(models.Model):
    _name = 'dti.business.subtype'

    name = fields.Char(string='Business Subtype name', required=True)
    active = fields.Boolean(default=True)


class DtiBusinessType(models.Model):
    _name = 'dti.business.type'

    name = fields.Char(string='Business type name', required=True)
    active = fields.Boolean(default=True)
