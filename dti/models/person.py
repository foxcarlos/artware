# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=broad-except

import logging
from odoo import models, fields

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

_logger = logging.getLogger(__name__)


class DtiSectorType(models.Model):
    _name = 'dti.sector'
    _description = 'Sector type'

    name = fields.Char(string='Sector name')
    description = fields.Char()
    active = fields.Boolean(default=True)


class DtiPersonEducation(models.Model):

    _name = 'dti.person.education'
    _description = 'Nivel de estudios'

    name = fields.Char()
    active = fields.Boolean(default=True)


class DtiPerson(models.Model):

    _name = 'dti.person'
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner')
    lastname = fields.Char()
    sector_ids = fields.Many2many('dti.sector', string='Sector Type')
    education = fields.Many2one('dti.person.education')
    twitter = fields.Char()
    facebook = fields.Char()
    instagram = fields.Char()
    linkedin = fields.Char()
    tag_ids = fields.Many2many('blog.tag', string='Blog Tag')
    reason_unsubscribe = fields.Char()
    unsubscribe = fields.Boolean()
    reason_unsubscribe = fields.Char()
    adherence_protocol = fields.Boolean()
    adherence_version = fields.Char()
    privacy_policies = fields.Boolean()
    privacy_version = fields.Char()
    active = fields.Boolean(default=True)

    def update_person(self, data):
        person = self.env['dti.person'].search([
            ('id', '=', data.get('person_id'))])

        name = data.get('name')
        lastname = data.get('lastname')
        identification_type = data.get('identification_type')
        vat = data.get('vat')
        birthday = data.get('birthday') or None
        street = data.get('street')
        state_id = data.get('state_id')
        city = data.get('city')
        email = data.get('email')
        mobile = data.get('mobile')
        sector_id = data.get('sector_id')
        sector_ids = self.env['dti.sector'].search([('id', 'in', sector_id)])
        education_id = data.get('education_id')
        twitter = data.get('twitter')
        facebook = data.get('facebook')
        instagram = data.get('instagram')
        linkedin = data.get('linkedin')
        intervention_id = data.get('intervention_ids')
        intervention_ids = self.env['dti.intervention'].search(
            [('id', 'in', intervention_id)])
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        try:
            person.write({
                'name': name,
                'lastname': lastname,
                'l10n_latam_identification_type_id': int(identification_type),
                'vat': vat,
                'birthday': birthday,
                'street': street,
                'state_id': state_id,
                'city': city,
                'email': email,
                'mobile': mobile,
                'sector_ids': sector_ids,
                'education': education_id,
                'twitter': twitter,
                'facebook': facebook,
                'instagram': instagram,
                'linkedin': linkedin,
                'intervention_ids': intervention_ids,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version})
        except Exception as error_update:
            _logger.error("{error_update}", error_update=error_update)
            return False
        return True

    def create_person(self, data):

        names = data.get('name')
        lastnames = data.get('lastname')
        identification_type = data.get('identification_type')
        vat = data.get('vat')
        birthday = data.get('birthday') or None
        street = data.get('street')
        state_id = data.get('state_id')
        city = data.get('city')
        mobile = data.get('mobile')
        email = data.get('email')
        sector_id = data.get('sector_id')
        sector_ids = self.env['dti.sector'].search([('id', 'in', sector_id)])
        education = data.get('education_id')
        education_id = self.env['dti.person.education'].search([(
            'id', 'in', education)])
        twitter = data.get('twitter')
        facebook = data.get('facebook')
        instagram = data.get('instagram')
        linkedin = data.get('linkedin')
        intervention_id = data.get('intervention_ids')
        adt_ids = self.env['dti.intervention'].search([
            ('name', 'in', intervention_id)])
        adherence_protocol = data.get('adherence_protocol')
        adherence_version = data.get('adherence_version')
        privacy_policies = data.get('privacy_policies')
        privacy_version = data.get('privacy_version')

        try:
            self.create({
                'name': names,
                'lastname': lastnames,
                'l10n_latam_identification_type_id': int(identification_type),
                'vat': vat,
                'birthday': birthday,
                'street': street,
                'state_id': state_id,
                'city': city,
                'mobile': mobile,
                'email': email,
                'sector_ids': sector_ids,
                'education': education_id.id if education_id else None,
                'twitter': twitter,
                'facebook': facebook,
                'instagram': instagram,
                'linkedin': linkedin,
                'intervention_ids': adt_ids if adt_ids else None,
                'adherence_protocol': adherence_protocol,
                'adherence_version': adherence_version,
                'privacy_policies': privacy_policies,
                'privacy_version': privacy_version,
            })
        except Exception as error_create:
            _logger.error("{error_create}", error_create=error_create)
            return False, error_create
        return True, ''

    def _import_data(self):
        persons = self.search([])

        for record in persons:
            record.l10n_latam_identification_type_id.name = 'VAT'
            rp_copy = record
            rp_id = record.partner_id.id if record.partner_id else ''
            rp = self.env['res.partner'].search([('id', '=', rp_id)])
            rp_copy.copy()
            record.unlink()
            rp.unlink()
            persons._cr.commit()
            rp._cr.commit()
