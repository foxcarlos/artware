from . import municipality
from . import business
from . import res_partner
from . import institution
from . import attraction
from . import person
