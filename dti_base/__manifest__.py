# -*- coding: utf-8 -*-
{
    'name': "dti_base",

    'summary': """
        Dependencias y configuraciones iniciales
        """,

    'description': """
        Para inicialización de la base de datos
    """,

    'author': "Rizzo/Garcia",
    'website': "http://redti.naltu.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'DTI',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'website',
                'website_blog',
                'website_event',
                'mass_mailing'
                ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
